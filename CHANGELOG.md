## v0.2.4 (2024-04-16)

### Fix

- 🚑️ fix undocumented zone detection

## v0.2.3 (2024-04-16)

### Fix

- 🐛 allow post code to be null

## v0.2.2 (2024-03-31)

### Fix

- 🐛 live api url

## v0.2.1 (2024-03-17)

### Fix

- 📌 pin minimum python version to 3.8
- 🐛 fix typing and bump python test image version
- 🐛 fix dependencies
- 📌 reduce python version to 3.9
- fix python comapatibility

## 0.2.0 (2024-03-14)

### Feat

- All the API ant tests
